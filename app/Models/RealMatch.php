<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RealMatch extends Model
{
    use HasFactory;
    protected $fillable = [
        'real_team_1',
        'real_team_2',
        'real_team_1_score',
        'real_team_2_score',
        'date',
        'status'
    ];
}

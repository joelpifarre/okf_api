<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leagues', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->boolean('isPublic')->default(true);
            $table->string('real_league');
            $table->enum('max_players',[8,12,16])->default(8);
            $table->enum('init_money',[50000.0,100000.0])->default(50000.0);
            $table->string('code')->nullable()->unique();
            $table->enum('max_tranf_player',[4,6,8])->default(4);
            $table->foreign('real_league')->references('code')->on('real_leagues')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leagues');
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\League;
use App\Models\UserLeague;
use App\Models\RealPlayer;
use App\Models\Roster;
use App\Http\Resources\League as LeagueResource;
use App\Http\Controllers\API\BaseController;
use App\Models\Market;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

use function PHPUnit\Framework\isNull;

class LeagueController extends BaseController
{
    //

    public function createLeague(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|unique:leagues|max:150',
            'isPublic' => 'required',
            'real_league' => 'required',
            'max_players' => 'required',
            'init_money' => 'required',
            'max_tranf_player' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            DB::beginTransaction();
            if ($data['isPublic'] == false){
                $data['code'] = $this->generate_private_code();
            }

            $league = League::create($data);
            $userLeagueData = [
                'league'=>$league->id,
                'user' => $user->id,
                'money' => $league->init_money,
                'role' =>'admin'
            ];

            $userLeague = UserLeague::create($userLeagueData);

            $this->generate_new_roster($league->id,$user->id);
            $m = new MarketController();
            $m->generateMarket($league->id);

            DB::commit();

        }catch (Exception $e){
            DB::rollback();
        }
        
        print($league);
        return $this->sendResponse(new LeagueResource($league), 'League created successfully');
        
       
    }

    public function readLeague(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();

        $league = UserLeague::select('leagues.id','leagues.name','leagues.isPublic','leagues.real_league','leagues.max_players','leagues.init_money','leagues.code','leagues.max_tranf_player','real_leagues.logo')
        ->join('leagues','user_leagues.league','=','leagues.id')
        ->join('real_leagues','real_leagues.code','=','leagues.real_league')
        ->where('user_leagues.user','=',$user->id)->get();

        return $this->sendResponse($league, 'All League List correct');
    }

    public function readOneLeague(Request $request){
        $user = Auth::user();
        $data = $request->all();

        $validator = Validator::make($data, [
            'league' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $league = UserLeague::select('leagues.*')->join('leagues', 'user_leagues.league', '=', 'leagues.id')->where('user_leagues.league', '=', $data['league'])->where('user_leagues.user', '=', $user->id)->first();

        return $this->sendResponse(new LeagueResource($league), 'Read league data correctly');
    }


    public function getLeagueConfiguration(Request $request){
        
    }
    public function getLeagueUsers(Request $request){
        $user = Auth::user();
        $data = $request->all();

        $validator = Validator::make($data, [
            'league' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        // $inLeague = UserLeague::where('user','=',$user->id)->where('league','=',$data['league'])->exists();
        // if (!$inLeague){
        //     return $this->sendError('Validation Error.','Unauthiruzed');
        // }

        $league =  UserLeague::select('users.name','users.email')->join('users','users.id','=','user_leagues.id')->where('user_leagues.league','=',$data['league'])->get();
        return $this->sendResponse(new LeagueResource($league), 'Read league data correctly');
    }
    
    public function joinLeague(Request $request){
        $data = $request->all();
        $user = Auth::user();
        
        $validator = Validator::make($data, [
            'code' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $league = League::select('*')->where('code','=',$data['code'])->first();


        if($league != null){

            try {
                DB::beginTransaction();
                $userLeagueData = [
                    'league' => $league->id,
                    'user' => $user->id,
                    'money' => $league->init_money,
                ];
                $userLeague = UserLeague::create($userLeagueData);
                // $this->generate_new_roster($league->id, $user->id);
                DB::commit();
                $this->generate_new_roster($league->id,$user->id);
                return $this->sendResponse(new LeagueResource($league),'Joined the league successfully');
            } catch (Exception $e) {
                DB::rollback();
                return $this->sendError('Error.', $e);
            }
    
            

        }else {
            return $this->sendError('Error.', $league);
        }


        
    }

    public function getNumOfLeagues(){
        $user = Auth::user();
        $numLeague = UserLeague::where('user','=',$user->id)->count();
        return $this->sendResponse($numLeague, 'Num of leagues ');
    }

    private function generate_private_code(){
       
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $leagueCode = '';
            for($c = 0; $c < 4; $c++){
                for ($i = 0; $i < 4; $i++) {
                    $leagueCode .= $characters[rand(0, $charactersLength - 1)];
                }
                if($c < 3){
                    $leagueCode .='-';
                }
            }

            return $leagueCode;
    }
    
    private function generate_new_roster($league,$user){        
        for($i = 0; $i <2;$i++)
        {
            $roster_players = Roster::select('player')->where('league', '=', $league)->get();
            $market_players = Market::select('player')->where('league','=', $league)->get();
            $disponible_players_fw = RealPlayer::select('id')->whereNotIn('id', $roster_players)->whereNotIn('id', $market_players)->where('position', '=', 'GK')->get();
            $player = rand(0, count($disponible_players_fw) - 1);
            $roster = [
                "league" => $league,
                "user" => $user,
                "player" => $disponible_players_fw[$player]['id']
            ];
            Roster::create($roster);
        }
        for($i = 0; $i < 4;$i++)
        {
            $roster_players = Roster::select('player')->where('league', '=', $league)->get();
            $market_players = Market::select('player')->where('league','=', $league)->get();
            $disponible_players_fw = RealPlayer::select('id')->whereNotIn('id', $roster_players)->whereNotIn('id', $market_players)->where('position', '=', 'DF')->get();
            $player = rand(0, count($disponible_players_fw) - 1);
            $roster = [
                "league" => $league,
                "user" => $user,
                "player" => $disponible_players_fw[$player]['id']
            ];
            Roster::create($roster);
        }
        for($i = 0; $i < 4;$i++)
        {
            $roster_players = Roster::select('player')->where('league', '=', $league)->get();
                        $market_players = Market::select('player')->where('league','=', $league)->get();
            $disponible_players_fw = RealPlayer::select('id')->whereNotIn('id', $roster_players)->whereNotIn('id', $market_players)->where('position', '=', 'FW')->get();
            $player = rand(0, count($disponible_players_fw) - 1);
            $roster = [
                "league" => $league,
                "user" => $user,
                "player" => $disponible_players_fw[$player]['id']
            ];
            Roster::create($roster);
        }
    }
}

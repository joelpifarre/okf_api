<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\LeagueController;
use App\Http\Controllers\API\RosterController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register',[AuthController::class, 'register']);
    Route::get('logout', [AuthController::class, 'logout'])->middleware(['auth:api']);
});


Route::group(['prefix'=>'league', 'middleware'=>'auth:api'], function(){
    Route::post('add',[LeagueController::class,'createLeague']);
    Route::post('join',[LeagueController::class,'joinLeague']);
    Route::post('get',[LeagueController::class,'readLeague']);
    Route::post('info',[LeagueController::class, 'readOneLeague']);
    Route::post('users',[LeagueController::class, 'getLeagueUsers']);
});

Route::group(['prefix'=>'roster', 'middleware'=>'auth:api'], function(){
    Route::post('get',[RosterController::class,'getUserRoster']);
    Route::post('pos',[RosterController::class,'getInitRoster']);
    Route::post('pos_filter',[RosterController::class,'getRosterByPosition']);
    Route::get('icon',[RosterController::class, 'getTeamIcon']);
    Route::post('change',[RosterController::class, 'changePlayerPos']);
    // Route::post('join',[LeagueController::class,'joinLeague']);
});
Route::group(['prefix'=>'user', 'middleware'=>'auth:api'], function(){
    Route::post('numleagues',[LeagueController::class, 'getNumOfLeagues']);
    // Route::post('join',[LeagueController::class,'joinLeague']);
});

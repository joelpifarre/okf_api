<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class League extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'isPublic',
        'real_league',
        'max_players',
        'init_money',
        'code',
        'max_tranf_player'
    ];

}

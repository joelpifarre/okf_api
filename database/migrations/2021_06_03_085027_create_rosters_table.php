<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRostersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rosters', function (Blueprint $table) {
            $table->id();
            $table->integer('user')->unsigned();
            $table->integer('league')->unsigned();
            $table->integer('player')->unsigned();
            $table->datetime('start')->useCurrent();
            $table->datetime('end')->nullable();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('league')->references('id')->on('leagues')->onDelete('cascade');
            $table->foreign('player')->references('id')->on('real_players')->onDelete('cascade');
            $table->enum('position',['FW1','FW2','DF1','DF2','GK','SFW','SDF','SGK'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rosters');
    }
}

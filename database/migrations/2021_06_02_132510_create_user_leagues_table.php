<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLeaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_leagues', function (Blueprint $table) {
            $table->id();
            $table->integer('league')->unsigned();
            $table->integer('user')->unsigned();
            $table->double('money');
            $table->enum('role',['player','admin'])->default('player');
            $table->foreign('league')->references('id')->on('leagues')->onDelete('cascade');
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_leagues');
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;

use App\Models\User;

use Illuminate\Support\Facades\Auth;

use Validator;

class AuthController extends BaseController
{
    //

    public function register(Request $request)
    {
       $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string',
            'c_password' => 'required|same:password',
       ]);

       if($validator->fails()){
           return $this->sendError('Validation error .', $validator->errors());
       }

       $input = $request->all();

       $input['password'] = bcrypt($input['password']);
       $user = User::create($input);
       $success['token'] = $user->createToken('MyApp')->accessToken;
       $success['name'] = $user->name;

       return $this->sendResponse($success,'User register successfully.');
    }

    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] = $user->email;

            return $this->sendResponse($success, 'User login successfully.');
        } else {
             return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }

    public function logout(Request $request)
    {
        // $user = Auth::check()->token();
        // $user->revoke();

        if (Auth::check()) {
                $request->user()->token()->revoke();
                return $this->sendResponse('','Successfully logged out');
         }

      
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealLeaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_leagues', function (Blueprint $table) {
            $table->id();
            $table->dropPrimary('id');
            $table->string('code',4);
            $table->string('name');
            $table->string('logo');
            $table->boolean('status')->default(false);
            $table->primary('code');
            $table->dropColumn('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_leagues');
    }
}

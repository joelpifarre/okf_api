<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\League;
use App\Models\Market;
use App\Models\RealPlayer;
use App\Models\Roster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MarketController extends BaseController
{

    public function getLeagueMarket(Request $request){
        $data = $request->all();
        $user = Auth::user();
        $market = Market::where('user','=',$user->id)->where('league','=',$data['league'])->where('status','=','tosell')->get(); 
        $this->sendResponse($market,'Market load successfull');
    }

    public function generateMarket($league){
        
        // $leagueData = League::where('id','=',$league)->get();
        // // Market::whereNull('user')->where('league', '=', $leagueData->id)->delete();

        // for($i = 0; $i < 4 ; $i++)
        // {
        //     $playersInRosters = Roster::select('player')->where('league', '=', $league)->get();
        //     $playersToTransfer = RealPlayer::select('id')->whereNotIn('id', $playersInRosters->player)->get();
        //     $transfer = rand(0,count($playersToTransfer)-1);
        //     $playerData = RealPlayer::where('id','=',$transfer)->first();
        //     $marketData = [
        //         'league'=>$leagueData->id,
        //         'player'=>$transfer,
        //         'price'=>$playerData->value,
        //         'status'=>'tosell'
        //     ];

        //     $newPlayerToSell = Market::create($marketData);
        // }
    }
    //
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\League;
use App\Models\RealPlayer;
use App\Models\Roster;
use App\Models\UserLeague;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class RosterController extends BaseController
{
    //
    public function getUserRoster(Request $request){

        $data = $request->all();
        $user = Auth::user();
        $validator = Validator::make($data, [
            'league' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
       
        $getUserRoster = Roster::select('real_players.id', 'real_players.name', 'real_players.lastname','real_players.position','real_players.real_team','real_teams.logo','real_players.image','rosters.position as initPos')
        ->where('rosters.user', '=',$user->id)->where('rosters.league','=',$data['league'])->whereNull('end')
        ->join('real_players','rosters.player','=','real_players.id')
        ->join('real_teams','real_teams.code','=','real_players.real_team')
        ->orderby('real_players.position','desc')
        ->get();
        return $this->sendResponse($getUserRoster ,'Roster successfully');
    }

    public function setPrimaryFive(Request $request){

    }

    public function getInitRoster(Request $request){
        $data = $request->all();
        $user = Auth::user();
        $validator = Validator::make($data, [
            'pos' => 'required',
            'league'=>'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
       
        $getUserRoster = Roster::select('real_players.id', 'real_players.name', 'real_players.lastname','real_players.position','real_players.real_team','real_teams.logo','real_players.image','rosters.position as initPos')
        ->where('rosters.user', '=',$user->id)->where('rosters.league','=',$data['league'])->where('rosters.position','=',$data['pos'])->whereNull('end')
        ->join('real_players','rosters.player','=','real_players.id')
        ->join('real_teams','real_teams.code','=','real_players.real_team')
        ->orderby('real_players.position','desc')
        ->first();
        return $this->sendResponse($getUserRoster ,'League created successfully');
    }
    public function getRosterByPosition(Request $request){
        $data = $request->all();
        $user = Auth::user();
        $validator = Validator::make($data, [
            'pos' => 'required',
            'league'=>'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
       
        $getUserRoster = Roster::select('real_players.id', 'real_players.name', 'real_players.lastname','real_players.position','real_players.real_team','real_teams.logo','real_players.image','rosters.position as initPos')
        ->where('rosters.user', '=',$user->id)->where('rosters.league','=',$data['league'])->where('real_players.position','=',$data['pos'])->whereNull('rosters.position')->whereNull('end')
        ->join('real_players','rosters.player','=','real_players.id')
        ->join('real_teams','real_teams.code','=','real_players.real_team')
        ->orderby('real_players.position','desc')
        ->get();
        return $this->sendResponse($getUserRoster ,'League created successfully');
    }


    public function getTeamIcon(Request $request){
        return url('storage/avatars/image.png');
    }

    public function changePlayerPos(Request $request){
        $data = $request->all();
        $user=Auth::user();
        $validator = Validator::make($data, [
            'pos' => 'required',
            'league'=>'required',
            'player'=>'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        Roster::where('league',$data['league'])->where('user',$user->id)->where('position',$data['pos'])->update(['position'=>null]);
        Roster::where('league',$data['league'])
        ->where('user',$user->id)
        ->where('player',$data['player'])
        ->update(['position'=>$data['pos']]);
        
        return $this->sendResponse('' ,'Changed Correctly');

    }
}

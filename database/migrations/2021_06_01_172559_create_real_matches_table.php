<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_matches', function (Blueprint $table) {
            $table->id();
            $table->string('real_team_1');
            $table->string('real_team_2');
            $table->integer('real_team_1_score');
            $table->integer('real_team_2_score');
             $table->foreign('real_team_1')->references('code')->on('real_teams')->onDelete('cascade');
             $table->foreign('real_team_2')->references('code')->on('real_teams')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_matches');
    }
}

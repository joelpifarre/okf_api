<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => "Administrator",
            'email' => "admin@okfantsay.com",
            'password'=> bcrypt("@dm1n"),
            'role' => "admin",
        ]);
    }
}

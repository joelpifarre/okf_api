<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealPlayerMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_player_matches', function (Blueprint $table) {
            $table->id();
            $table->integer('real_match')->unsigned();
            $table->integer('real_player')->unsigned();
            $table->integer('goal');
            $table->integer('assist');
            $table->integer('blue_card');
            $table->integer('red_card');
            $table->integer('direct_foult');
            $table->integer('direct_foult_fail');
            $table->integer('penalty');
            $table->integer('penalty_fail');
            $table->integer('penalty_attachment');
            $table->integer('direct_fault_attachment');
            $table->integer('points');
            $table->foreign('real_match')->references('id')->on('real_matches')->onDelete('cascade');
            $table->foreign('real_player')->references('id')->on('real_players')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_player_matches');
    }
}

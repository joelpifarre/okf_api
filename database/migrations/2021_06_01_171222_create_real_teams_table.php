<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_teams', function (Blueprint $table) {
            $table->id();
            $table->dropPrimary('id');
            $table->string('code',4);
            $table->string('name');
            $table->string('logo');
            $table->string('real_league');
            $table->foreign('real_league')->references('code')->on('real_leagues')->onDelete('cascade');
            $table->primary('code');
            $table->dropColumn('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_teams');
    }
}

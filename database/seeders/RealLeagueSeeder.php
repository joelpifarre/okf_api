<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RealLeagueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // INSERT REAL LEAGUE
        DB::table('real_leagues')->insert([
            'code' => "OKF",
            'name' => "OK Liga Femenina",
            'logo' => "logo",
            'status' => true,
        ]);
        for ($i=0; $i < 17; $i++) {
            DB::table('real_teams')->insert([
                'code' => "TM".$i,
                'name' => "Team ".$i,
                'logo' => "A",
                'real_league' => "OKF",
            ]);
            for ($p=0; $p < 2 ; $p++) {
                DB::table('real_players')->insert([
                'name' => "Player TM".$i."  P".$p,
                'lastname' => "-",
                'position' => "GK",
                'real_team' => "TM".$i,
                'value'=>1000
                ]);
            }
            for ($d=2; $d < 6; $d++) {
                DB::table('real_players')->insert([
                    'name' => "Player TM" . $i . "  P" . $d,
                    'lastname' => "-",
                    'position' => "DF",
                    'real_team' => "TM" . $i,
                    'value' => 1000
                ]);
            }
            for ($f=2; $f < 10; $f++) {
                DB::table('real_players')->insert([
                    'name' => "Player TM" . $i . "  P" . $f,
                    'lastname' => "-",
                    'position' => "FW",
                    'real_team' => "TM" . $i,
                    'value'=>1000
                ]);

                print("Player TM" . $i . "  P" . $f);
            }
        }

    }
}
